// To allow vue devtools in Chrome
Vue.config.devtools = true;

// Tasks Component
Vue.component('tasks', {
    template: 
        `<section class="todoapp">
            <header class="header">
                <h1>TASK LIST</h1>
                <input v-on:keyup.enter="add" v-model="newTask" type="text" class="new-todo" placeholder="What are you going to do?">
            </header>
            <section>
                <ul class="todo-list">
                    <li class="todo" is="task" v-for="task in tasks" v-bind:task="task"></li>
                </ul>
            </section>

            <footer class="footer" v-show="tasks.length">
                <span class="todo-count">Completed tasks: {{ completed }} | Uncompleted tasks: {{ uncompleted }}</span>
            </footer>
        </section>`,
    data: function() {
        return {
            name: "",
            tasks: [
                {title: "Learning VueJS", completed: true},
                {title: "Learning Laravel", completed: false},
                {title: "Learning Bootstrap", completed: false}                 
            ],
            newTask: ""
        }
    },
    methods: {
        add: function() {
            if(this.newTask.length <= 1) { return }
            this.tasks.push({title: this.newTask,
                completed: false
            });
            // We refresh the value
            this.newTask = "";
        }
    },
    computed: {
        completed: function() {
            return this.tasks.filter(function(task) {
                return task.completed;
            }).length;
        },
        uncompleted: function() {
            return this.tasks.filter(function(task) {
                return !task.completed;
            }).length;
        }
    }
});

// Task Component
Vue.component('task', {
    template: ` 
    <li :class="classes">
        <div class="view">
            <input class="toggle" type="checkbox" v-model="task.completed"/>
            <label v-text="task.title" @dblclick="edit()"></label>
            <button class="destroy" @click="remove()"></button>
        </div>
        <input class="edit" 
            v-model="task.title"
            @keyup.enter="doneEdit()"
            @blur="doneEdit()"
            @keyup.esc="cancelEdit()"
        />
    </li>
        `,
    props: ['task'],
    data: function() {
        return {
            editable: false,
            cacheBeforeEdit: ''
        }
    },
    methods: {
        edit: function() {
            this.cacheBeforeEdit = this.task.title;
            this.editable = true;
        },
        cancelEdit: function() {
            this.editing = false;
            this.task.title = this.cacheBeforeEdit;
        },
        doneEdit: function() {
            if(!this.task.title) {
                this.remove();
            }
            this.editable = false;
        },
        remove: function() {
            var tasks = this.$parent.tasks;

            tasks.splice(tasks.indexOf(this.task), 1)
        }
    },
    computed: {
        classes: function() {
            return {
                completed: this.task.completed,
                editing: this.editable
            };
        }

    }
})

// The Vue Instance
var vm = new Vue({
    el: "#app",
});